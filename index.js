console.log("Hello")

let assignNumber=7;

assignNumber+=2;

console.log(assignNumber);

let pemdas = 1+2+3*7/7;
console.log(pemdas);

/* Arithmetic orperators also includes increment and decrement operators
*/

let numE = true + 1;
console.log(numE + " - " + typeof(numE));

let numF = false +1;
console.log(numF + " - " + typeof(numF));

// Comparison operators (== - equality operator checks two eperands are the same
console.log("Comparison Operators")
console.log(1==1);
console.log(1=='1');
console.log	(0==false);
console.log('juan'=='Juan');

// Strict equality operator ===
console.log("Strict equality operators")
console.log(1===1);
console.log(1==='1');
console.log	(0===false);
console.log('juan'==='Juan');

// Inequality operator !=
console.log("Inequality operators")
console.log(1!=1);
console.log(1!='1');
console.log	(0!=false);
console.log('juan'!='Juan');

// Strict Inequality operator !=
console.log("Strict Inequality operators")
console.log(1!==1);
console.log(1!=='1');
console.log	(0!==false);
console.log('juan'!=='Juan');

// Relational Comparison Operator
console.log("Relational Comparisin Operators")
let x = 500;
let y = 700;
let w = 8000;
let numString = "5500";
console.log("Greater than (>)")
console.log(x > y);
console.log(y < x);
console.log(numString<6000);
console.log(numString<1000);

console.log("Greater than or equal to >=")
console.log(w<=w);
console.log




// Look for more lessons missed





// Conditional statement
if (true) {
	console.log("We just run an if condition");
}

let numG = 5;
if (numG<10) {
	console.log("Hello")
}

let user1 = "Ailen";
let userLevel = 25;
let userAge3 =20;
let isRegistered = true;
let isAdmin = false;

if(user1.length >= 10 && isRegistered && isAdmin){
	console.log("Welcome to the game!");
} else {
	console.log("You are not yet ready!");
}


// Create log-in function


function login(username,password) {
	if(typeof username=== "string" && typeof password === "string"){
		console.log("Both arguments are string.");
		
		// Mini Activity
		if (username.length>=8 && password.length>=8) {
			console.log("Thank you for logging in");

		} else if (username.length<=7){
			
			console.log("username too short")
			if (password.length<=7) {
				console.log("password too short as well!")
			}

		}else if (password.length<=7) {
			console.log("password too short");
		}else {		
			console.login("Credentials too short");

		}

	} else{
		console.log("one or both of the arguments are false");
	}
}

console.log(login("ailen","laguda"));


// Template Literals

console.log(`${"ailen"} ${"samson"} ${"laguda"}`);

// Ternary operator (ES6)

console.log(7>1?"true":"false");

let age1 = parseInt(prompt("Input Age:"));

function verifyAge(age1) {
	return age1>=18?console.log("You are not a minor"):console.log("You are underage");
}

verifyAge(age1);

// Switch Statement
/*
	- alternative for an if-else if- else statement
	Syntx:
		Swtich (expression){
			Case value1:
				statement;
				break
			Case value2:
				statement
				break;
			default:
				statement;
		}
	
*/

let day=prompt("What day is today?").toLowerCase();

switch(day){
	case 'monday':
		console.log('Color for today is red');
		break;
	case 'tuesday':
		console.log("Color for today is orange");
		break;
	case 'wednesday':
		console.log("Color for today is yellow");
		break;
	case 'thursday':
		console.log("Color for today is green");
		break;
	case 'friday':
		console.log("Color for today is blue");
		break;
	case 'saturday':
		console.log("Color for today is red");
		break;
	case 'sunday':
		console.log("Color for today is red");
		break;
	default:
		console.log("Please input a valid day")
}

// Try-Catch-Finally Statement

function showIntensityAlert(windSpeed){
	try{
		// attempt to execute a code
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch(error){
		console.log(typeof error);
		console.log(error.message);
	}
	finally{
		// Continue to execure code regardless of the sucess of failure
		alert('Intensity updates will show new Alert')
	}
}

showIntensityAlert(65)